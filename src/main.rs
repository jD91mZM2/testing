use std::{
    fs::File,
    io::prelude::*,
    os::unix::io::AsRawFd,
    thread,
    time::Duration,
};
use syscall::*;

fn main() {
    if unsafe { clone(CloneFlags::empty()).unwrap() } == 0 {
        let mut scheme = File::create(":testing").unwrap();
        let mut instance = DummyScheme;
        loop {
            let mut packet = Packet::default();
            scheme.read_exact(&mut packet).unwrap();
            instance.handle(&mut packet);
            scheme.write_all(&packet).unwrap();
        }
    } else {
        thread::sleep(Duration::from_secs(1));
        let file = File::open("testing:").unwrap();
        unsafe {
            fmap2(file.as_raw_fd() as usize, &Map2 {
                offset: 5,
                size: 0x3000,
                flags: PROT_WRITE | PROT_READ | MAP_FIXED_NOREPLACE,
                address: 0x20000000,
            }).expect("fmap2 failed");
            fmap2(file.as_raw_fd() as usize, &Map2 {
                offset: 5,
                size: 0x1000,
                flags: PROT_WRITE | PROT_READ | MAP_FIXED_NOREPLACE,
                address: 0x20003000,
            }).unwrap();

            funmap2(0x20001000, 0x1000).unwrap();

            // We should now have 3 mappings:
            // - 0x20000000..0x20001000
            // - 0x20002000..0x20003000
            // - 0x20003000..0x20004000
            funmap2(0x20000000, 0x4000).unwrap();
            thread::sleep(Duration::from_secs(1));
            println!("goodbye");
        }
    }
}

struct DummyScheme;

impl SchemeMut for DummyScheme {
    fn open(&mut self, _path: &[u8], _flags: usize, _uid: u32, _gid: u32) -> Result<usize> {
        Ok(0)
    }

    fn fmap2(&mut self, _id: usize, map: &Map2) -> Result<usize> {
        let boxed = vec![0u8; map.size + 4096].into_boxed_slice();
        let ptr = Box::into_raw(boxed) as *mut u8 as usize;
        let next_ptr = ptr + 4095;
        let aligned = next_ptr - next_ptr % 4096;
        println!("fmap({:#x?}) = {:#x}", map, aligned);
        Ok(aligned)
    }

    fn funmap2(&mut self, address: usize, length: usize) -> Result<usize> {
        println!("funmap({:#x}, {:#x})", address, length);
        Ok(0)
    }
}
